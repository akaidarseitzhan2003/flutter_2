// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Живой`
  String get alive {
    return Intl.message(
      'Живой',
      name: 'alive',
      desc: '',
      args: [],
    );
  }

  /// `Авторизация`
  String get auth {
    return Intl.message(
      'Авторизация',
      name: 'auth',
      desc: '',
      args: [],
    );
  }

  /// `Закрыть`
  String get close {
    return Intl.message(
      'Закрыть',
      name: 'close',
      desc: '',
      args: [],
    );
  }

  /// `Значение счетчика`
  String get counterValue {
    return Intl.message(
      'Значение счетчика',
      name: 'counterValue',
      desc: '',
      args: [],
    );
  }

  /// `Мертвый`
  String get dead {
    return Intl.message(
      'Мертвый',
      name: 'dead',
      desc: '',
      args: [],
    );
  }

  /// `Английский`
  String get english {
    return Intl.message(
      'Английский',
      name: 'english',
      desc: '',
      args: [],
    );
  }

  /// `Найти персонажа`
  String get findPerson {
    return Intl.message(
      'Найти персонажа',
      name: 'findPerson',
      desc: '',
      args: [],
    );
  }

  /// `Проверьте логин`
  String get inputErrorCheckLogin {
    return Intl.message(
      'Проверьте логин',
      name: 'inputErrorCheckLogin',
      desc: '',
      args: [],
    );
  }

  /// `Проверьте пароль`
  String get inputErrorCheckPassword {
    return Intl.message(
      'Проверьте пароль',
      name: 'inputErrorCheckPassword',
      desc: '',
      args: [],
    );
  }

  /// `Логин должен содержать не менее 3 символов`
  String get inputErrorLoginIsShort {
    return Intl.message(
      'Логин должен содержать не менее 3 символов',
      name: 'inputErrorLoginIsShort',
      desc: '',
      args: [],
    );
  }

  /// `Пароль должен содержать не менее 8 символов`
  String get inputErrorPasswordIsShort {
    return Intl.message(
      'Пароль должен содержать не менее 8 символов',
      name: 'inputErrorPasswordIsShort',
      desc: '',
      args: [],
    );
  }

  /// `Введите логин и пароль`
  String get inputLoginAndPassword {
    return Intl.message(
      'Введите логин и пароль',
      name: 'inputLoginAndPassword',
      desc: '',
      args: [],
    );
  }

  /// `Язык`
  String get language {
    return Intl.message(
      'Язык',
      name: 'language',
      desc: '',
      args: [],
    );
  }

  /// `Логин`
  String get login {
    return Intl.message(
      'Логин',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Нет данных`
  String get noData {
    return Intl.message(
      'Нет данных',
      name: 'noData',
      desc: '',
      args: [],
    );
  }

  /// `Пароль`
  String get password {
    return Intl.message(
      'Пароль',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Всего персонажей: {total}`
  String personsTotal(Object total) {
    return Intl.message(
      'Всего персонажей: $total',
      name: 'personsTotal',
      desc: '',
      args: [total],
    );
  }

  /// `Русский`
  String get russian {
    return Intl.message(
      'Русский',
      name: 'russian',
      desc: '',
      args: [],
    );
  }

  /// `Вход`
  String get signIn {
    return Intl.message(
      'Вход',
      name: 'signIn',
      desc: '',
      args: [],
    );
  }

  /// `Попробуйте снова`
  String get tryAgain {
    return Intl.message(
      'Попробуйте снова',
      name: 'tryAgain',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'ru', countryCode: 'RU'),
      Locale.fromSubtags(languageCode: 'en'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
